#!/usr/bin/python3
###############################################################################
'''电子模拟滤波器设计工具的主函数文件'''
# Copyright (c) 2022 Xu Ruijun | 1687701765@qq.com
#
# This file is part of Electronic Analog Filter Design Tool(eAFDTool).
#
# eAFDTool is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or any later version.
#
# eAFDTool is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# eAFDTool. If not, see <https://www.gnu.org/licenses/>.
###############################################################################
import time
import curses

from scipy import signal

from tui.selection import InitSelection
from tui import sele2other

__author__ = "Xu Ruijun"
__copyright__ = "Copyright (c) 2022 Xu Ruijun"
__license__ = "GPLv3 or later"
__email__ = "1687701765@qq.com"
__url__ = "https://gitee.com/xu-ruijun/eafdtool"


def main(stdscr):
    sel = InitSelection(stdscr)
    sel.init_win()
    r = sel.run(stdscr)
    if isinstance(r, tuple):
        ftype, frads, ffit, nord, topo = r
    else:
        exit()

    stdscr.erase()

    # 支持的滤波器列表：请查看 tui/sele2other.py
    B, A = sele2other.param2BA(ftype, frads, ffit, nord)
    f_solver, f_adjs = sele2other.param2func(ftype, nord, topo)
    solver = f_solver(B, A)
    adjs = f_adjs(solver)
    adjs.run(stdscr)


if __name__ == '__main__':
    curses.wrapper(main)
