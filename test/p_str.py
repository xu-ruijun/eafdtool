#!/usr/bin/python3
##################################################
'''字符串解析测试'''
# Copyright (c) 2022 Xu Ruijun | 1687701765@qq.com
# the file is MIT License
##################################################
import unittest

from tui.parser_str import parser_SInum, parser_SInum_unit

__author__ = "Xu Ruijun"
__copyright__ = "Copyright (c) 2022 Xu Ruijun"
__license__ = "MIT"


class pSInum_TestCase(unittest.TestCase):
    def test_base(self):
        self.assertEqual(parser_SInum('0.1'), 0.1)
        self.assertEqual(parser_SInum('.1'), 0.1)
        self.assertEqual(parser_SInum('1'), 1.)
        self.assertEqual(parser_SInum('1.'), 1.)
        self.assertEqual(parser_SInum('123456'), 123456.)
        self.assertEqual(parser_SInum('3.14159'), 3.14159)

    def test_SIprefix(self):
        self.assertEqual(parser_SInum('1k'), 1e3)
        self.assertEqual(parser_SInum('1k1'), 1.1e3)
        self.assertEqual(parser_SInum('1.5k'), 1.5e3)
        self.assertEqual(parser_SInum('1μ'), 1e-6)
        self.assertEqual(parser_SInum('1m'), 1e-3)
        self.assertEqual(parser_SInum('10m'), 0.01)
        self.assertEqual(parser_SInum('1M'), 1e6)

    def test_exps(self):
        self.assertEqual(parser_SInum('1.23e2'), 1.23e2)
        self.assertEqual(parser_SInum('1.23E2'), 1.23E2)
        self.assertEqual(parser_SInum('1.23e+2'), 1.23e+2)
        self.assertEqual(parser_SInum('1e-9'), 1e-9)
        self.assertEqual(parser_SInum('10e-3'), 10e-3)
        self.assertEqual(parser_SInum('0.1e-3'), 0.1e-3)

    def test_mixed(self):
        self.assertEqual(parser_SInum('1e3k'), 1e6)
        self.assertEqual(parser_SInum('1e+3m'), 1)
        self.assertEqual(parser_SInum('1e-3k'), 1)

    def test_space(self):
        self.assertEqual(parser_SInum('1.23 '), 1.23)
        self.assertEqual(parser_SInum(' 1.23'), 1.23)
        self.assertEqual(parser_SInum(' 1.23 '), 1.23)
        self.assertEqual(parser_SInum('  1.23  '), 1.23)

    def test_errs(self):
        with self.assertRaises(Exception):
            parser_SInum('1x1')
        with self.assertRaises(Exception):
            parser_SInum('1kk1')
        with self.assertRaises(Exception):
            parser_SInum('1.3k1')
        with self.assertRaises(Exception):
            parser_SInum('0.1.2')
        with self.assertRaises(Exception):
            parser_SInum('abcd')
        with self.assertRaises(Exception):
            parser_SInum('1.2x')
        with self.assertRaises(Exception):
            parser_SInum('x1.2')
        with self.assertRaises(Exception):
            parser_SInum('一二三')


class pSInuint_TestCase(unittest.TestCase):
    def test_all(self):
        units = ['m', 'mmHg', 'mol', 'N', 'Pa']
        self.assertEqual(parser_SInum_unit('1', units), (1., ''))
        self.assertEqual(parser_SInum_unit('1.23', units), (1.23, ''))

        self.assertEqual(parser_SInum_unit('1m', units), (1., 'm'))
        self.assertEqual(parser_SInum_unit('1mm', units), (0.001, 'm'))
        self.assertEqual(parser_SInum_unit('750mmHg', units), (750., 'mmHg'))

        self.assertEqual(parser_SInum_unit('1mol', units), (1., 'mol'))
        self.assertEqual(parser_SInum_unit('1mmol', units), (0.001, 'mol'))
        self.assertEqual(parser_SInum_unit('1mmol/L', units), (0.001, 'mol/L'))

        self.assertEqual(parser_SInum_unit('101325Pa', units), (101325., 'Pa'))
        self.assertEqual(parser_SInum_unit('101.325kPa', units), (101325., 'Pa'))
        self.assertEqual(parser_SInum_unit('1mPa', units), (0.001, 'Pa'))
        self.assertEqual(parser_SInum_unit('1.m/s', units), (1., 'm/s'))
        self.assertEqual(parser_SInum_unit('10mm/s', units), (0.01, 'm/s'))

        self.assertEqual(parser_SInum_unit('9.8N/kg', units), (9.8, 'N/kg'))
        #self.assertEqual(parser_SInum_unit('9.8n/kg', units), (9.8e-9, '/kg'))
        self.assertEqual(parser_SInum_unit('2N.m', units), (2., 'N.m'))
        self.assertEqual(parser_SInum_unit('2nm', units), (2e-9, 'm'))
        self.assertEqual(parser_SInum_unit('2nN', units), (2e-9, 'N'))

        self.assertEqual(parser_SInum_unit('1Ω', units), (1., 'Ω'))
        self.assertEqual(parser_SInum_unit('0.1ohm', units), (0.1, 'ohm'))
        self.assertEqual(parser_SInum_unit('1k ohm', units), (1000., 'ohm'))
        self.assertEqual(parser_SInum_unit('1k1 ohm', units), (1100., 'ohm'))
        self.assertEqual(parser_SInum_unit('1.1k ohm', units), (1100., 'ohm'))
        self.assertEqual(parser_SInum_unit('1.1 kohm', units), (1100., 'ohm'))

        self.assertEqual(parser_SInum_unit('0.001kWh', units), (1., 'Wh'))
        units.append('kWh')
        self.assertEqual(parser_SInum_unit('1kWh', units), (1., 'kWh'))
        self.assertEqual(parser_SInum_unit('0.001kWh', units), (0.001, 'kWh'))
        self.assertEqual(parser_SInum_unit('1000kWh', units), (1000., 'kWh'))
