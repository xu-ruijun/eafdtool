#!/usr/bin/python3
###############################################################################
'''UI选择参数转换为其他'''
# Copyright (c) 2022 Xu Ruijun | 1687701765@qq.com
#
# This file is part of Electronic Analog Filter Design Tool(eAFDTool).
#
# eAFDTool is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or any later version.
#
# eAFDTool is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# eAFDTool. If not, see <https://www.gnu.org/licenses/>.
###############################################################################
from scipy import signal
from algo import solvers
from tui import tui

__author__ = "Xu Ruijun"
__copyright__ = "Copyright (c) 2022 Xu Ruijun"
__license__ = "GPLv3 or later"


def param2BA(ftype, frads, ffit, nord):
    if ftype in {'bandpass', 'bandstop'}:
        nord /= 2
    if ffit > 0:
        B, A = signal.cheby1(nord, ffit, frads, ftype, analog=True)
    else:
        B, A = signal.butter(nord, frads, ftype, analog=True)
        if ffit > 0:
            b, a = signal.bessel(nord, frads, ftype, analog=True)
            assert (B == b).all()
            A += a
    return B, A

def param2func(ftype, nord, topo):
    d = {
    ('lowpass',  2, 'sk' ): (solvers.SK_LPF2,  tui.SK_Adjs),
    ('highpass', 2, 'sk' ): (solvers.SK_HPF2,  tui.SK_Adjs),
    ('lowpass',  3, 'sk' ): (solvers.SK_LPF3,  tui.SK_Adjs),
    ('lowpass',  2, 'mfb'): (solvers.MFb_LPF2, tui.SK_Adjs),
    ('highpass', 2, 'mfb'): (solvers.MFb_HPF2, tui.SK_Adjs),
    ('bandpass', 2, 'mfb'): (solvers.MFb_BPF2, tui.SK_Adjs)}
    k = (ftype, nord, topo)
    if k in d:
        return d[k]
    else:
        raise KeyError(f'暂时还没有实现这种滤波器: {ftype} {nord}order {topo}')
