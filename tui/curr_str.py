#!/usr/bin/python3
###############################################################################
'''一些Unicode方块和字符画'''
# Copyright (c) 2022 Xu Ruijun | 1687701765@qq.com
#
# This file is part of Electronic Analog Filter Design Tool(eAFDTool).
#
# eAFDTool is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or any later version.
#
# eAFDTool is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# eAFDTool. If not, see <https://www.gnu.org/licenses/>.
###############################################################################

__author__ = "Xu Ruijun"
__copyright__ = "Copyright (c) 2022 Xu Ruijun"
__license__ = "GPLv3 or later"


vblocks = '  ▎▍▌▋▊▉█'
hblocks = ' ▁▂▃▄▅▆▇█'
grays = ' ░▒▓█'
corn4 = ' ▗▖▄▝▐▞▟▘▚▌▙▀▜▛█'

lpf = '''\
__   
  \__
 LPF'''

hpf = '''\
   __
__/  
 HPF'''

bpf = '''\
  _  
_/ \_
 BPF'''

bsf = '''\
_   _
 \_/ 
 BSF'''

xxf = [lpf, hpf, bpf, bsf]
nxf = ['lowpass', 'highpass', 'bandpass', 'bandstop']

sk_lpf2 = '''\
                          C1
             ------------| |----------
             |                       |
       ____  |  ____        | \      |
In +--| R1 |---| R2 |-------| + \    |
                     _|_    |     >--|---+ Out
                     ___  --| - /    |
                      |   | | /      |
                    __|__ ------------
                     ___
                      _               '''

sk_lpf3 = '''\
                                   C2
                      ------------| |----------
                      |                       |
       ____     ____  |  ____        | \      |
In +--| R1 |---| R2 |---| R3 |-------| + \    |
            _|_               _|_    |     >--|---+ Out
         C2 ___            C3 ___  --| - /    |
             |                 |   | | /      |
           __|__             __|__ ------------
            ___               ___
             _                 _               '''

sk_hpf2 = '''\
                        ____
           ------------| R1 |--------
           |                        |
       C1  |   C2          | \      |
In +--| |-----| |----------| + \    |
                     |     |     >--|---+ Out
                    | |  --| - /    |
                    |_|  | | /      |
                   __|__ ------------
                    ___
                     _               '''

sk_hpf3 = '''\
                                ____
                   ------------| R2 |--------
                   |                        |
       C1      C2  |   C3          | \      |
In +--| |-----| |-----| |----------| + \    |
           |                 |     |     >--|---+ Out
       R2 | |            R3 | |  --| - /    |
          |_|               |_|  | | /      |
         __|__             __|__ ------------
          ___               ___
           _                 _               '''

