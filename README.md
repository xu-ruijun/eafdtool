# 电子模拟滤波器设计工具
**Election Analog Filter Desgin Tool**

## 功能
- 低通，高通，带通2阶/3阶有源滤波器设计
- 交互式调节电路参数

## 使用方法
测试环境：Ubuntu20.04 LTS, Python 3.8.10  
运行main.py  
1. 选择滤波器参数  
   用上下键切换所要改变的项  
   数值可以直接按键盘输入  
   注意: 不能随便选取滤波器参数，请查看tui/sele2other.py支持哪些滤波器  
         使用切比雪夫近似可能会使初始值为负数  
   切换到最下面的OK键按回车进入第二步  
2. 调节时间常数  
   用上下键切换，左右键改变数值大小  
   由于独立变量个数少于项数，在调节时其他项也会跟着一起动  
   不要让任何一项出现负值  
3. 调节RC大小  
   按Tab键切换到调节RC(再按一次可以回到调节时间常数)  
   按上下键切换到所需的项  
   可以用左右键调节或直接输入  
   注意：时间常数固定后，RC数值只有一个自由度，所以最多只能指定一个元件的值  
         暂时不支持调节RC来调节时间常数  

## 架构
Python语言开发  
用scipy signal模块生成传递函数  
参数求解器算法库  
用curses库实现文字图形界面  
算法和界面分离  

## 算法说明
三阶滤波器的传递函数是自己推的，考虑了后级电流对第一级RC滤波影响  
由于太忙，有些滤波器还没有仿真过，可能会有些错误。如果误差太大，请联系我  

## 未来计划
- 在调节界面显示电路图和综合信息
- 使用PyQt或PyGTK做GUI界面
- KiCAD接口，一键生成原理图(Eeschema不支持插件，只能生成原理图了)
- 元件参数优化，在已有元件值中寻找最小误差组合，计划在另一个程序中实现

## 为什么要做这款工具
- 一些电子设计工具是在网上的，个人认为有点不可靠，用户断网或停止运营，则无法继续使用
- 而且这些工具一般不是开源的，用户不能自己改进
- 我喜欢用的EDA工具KiCAD，里面缺乏这些工具，网上也找不到这些插件

## 版权
大部分文件采用GPLv3或更新，一些基本库(equation.py, expr2str.py等文件)采用MIT授权，无特别声明的文件默认采用GPLv3或更新授权。  
在原有文件上提交修改，默认采用与原文件相同的许可证授权，新文件则默认采用GPLv3或更新授权。提交者如需声明版权，请在文件头部添加声明。  

本程序是自由软件：你可以再分发之和/或依照由自由软件基金会发布的 GNU 通用公共许可证修改之，无论是版本 3 许可证，还是任何以后版都可以。  
发布该程序是希望它能有用，但是并无保障;甚至连可销售和符合某个特定的目的都不保证。请参看 GNU 通用公共许可证，了解详情。  
你应该随程序获得一份 GNU 通用公共许可证的复本。如果没有，请看 <https://www.gnu.org/licenses/>。  

## 资源链接和联系方式
[gitee](https://gitee.com/xu-ruijun/eafdtool)  
email: 1687701765@qq.com  

## 参考资料
- [ADI滤波器设计工具](https://tools.analog.com/cn/filterwizard/)
- [OKAWA Electric Design](http://sim.okawa-denshi.jp/en/Fkeisan.htm)
- [PyFDA Python(数字)滤波器设计分析工具](https://pypi.org/project/pyfda/)
