#!/usr/bin/python3
###############################################################################
'''2阶Sallen-Key滤波器参数求解'''
# Copyright (c) 2022 Xu Ruijun | 1687701765@qq.com
#
# This file is part of Electronic Analog Filter Design Tool(eAFDTool).
#
# eAFDTool is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or any later version.
#
# eAFDTool is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# eAFDTool. If not, see <https://www.gnu.org/licenses/>.
###############################################################################
import math

from .basic_solver import RC_filter

__author__ = "Xu Ruijun"
__copyright__ = "Copyright (c) 2022 Xu Ruijun"
__license__ = "GPLv3 or later"


class SK_LPF2(RC_filter):
    rx   = [0, 1]
    cx   = [2, 3]
    rcn  = ['r1', 'r2', 'c1', 'c2']
    t2rc = [[0, 2], [1, 3], [0, 3]]
    tname= ['R1C1', 'R2C2', 'R1C2']
    tchr = ['x',    'y',    'w']

    def t_adj(self, **kwargs):
        a, b = self.A[:2]
        ivars = self.process_relat_vars(kwargs, 'xyw')
        assert ivars.count(None) == 2
        x, y, w = ivars
        if w is not None:
            y = b - w
            x = a / y
        else:
            if x is not None:
                y = a / x
            elif y is not None:
                x = a / y
            w = b - y
        self.t = [x, y, w]
        return self.t

    def initv(self):
        a, b = self.A[:2]
        x = math.sqrt(a)
        return self.t_adj(x=x)


class SK_HPF2(RC_filter):
    rx   = [0, 1]
    cx   = [2, 3]
    rcn  = ['r1', 'r2', 'c1', 'c2']
    t2rc = [[0, 2], [1, 3], [1, 2]]
    tname= ['R1C1', 'R2C2', 'R1C2']
    tchr = ['x',    'y',    'u'   ]

    def t_adj(self, **kwargs):
        a, b = self.A[:2]
        ivars = self.process_relat_vars(kwargs, 'xyu')
        assert ivars.count(None) == 2
        x, y, u = ivars
        if u is not None:
            x = b - x
            y = a / x
        else:
            if y is not None:
                x = a / y
            elif x is not None:
                y = a / x
            u = b - x
        self.t = [x, y, u]
        return self.t

    def initv(self):
        a, b = self.A[:2]
        x = math.sqrt(a)
        return self.t_adj(x=x)
