#!/usr/bin/python3
###############################################################################
'''求解器测试'''
# Copyright (c) 2022 Xu Ruijun | 1687701765@qq.com
#
# This file is part of Electronic Analog Filter Design Tool(eAFDTool).
#
# eAFDTool is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or any later version.
#
# eAFDTool is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# eAFDTool. If not, see <https://www.gnu.org/licenses/>.
###############################################################################
import numpy as np
import matplotlib.pyplot as plt
from scipy import signal

import param_solve

__author__ = "Xu Ruijun"
__copyright__ = "Copyright (c) 2022 Xu Ruijun"
__license__ = "GPLv3 or later"


B, A = signal.butter(3, 1, 'low', analog=True)
lpf3 = param_solve.SK_LPF3(B, A)

# 搜索正数解，用于验证可行性
def find_positive(N):
    for i in range(1, N):
        x = i/100
        print(x)
        for j in range(1, N):
            y = j/100
            try:
                r = lpf3.t_adj(x=x, y=y)
            except Exception:
                pass
            else:
                if min(r) > 0:
                    return (x, y)

# 画出x,y二维图像
def draw_2d(N):
    arr = np.zeros([N, N])
    for i in range(1, N):
        x = i/100
        print(x)
        for j in range(1, N):
            y = j/100
            try:
                r = lpf3.t_adj(x=x, y=y)
            except Exception:
                pass
            else:
                if min(r) > 0:
                    arr[i, j] = 2  # 正实数解
                else:
                    arr[i, j] = 1  # 非正实数解
    plt.imshow(arr.transpose())
    plt.show()

if __name__ == '__main__':
    '''
    result = find_positive(200)
    if result is not None:
        x, y = result
        print(f'x={x}, y={y}')
        r = lpf3.t_adj(x=x, y=y)
        print('solve =', r)
    else:
        print('没有找到正实数解')
    '''
    draw_2d(200)
