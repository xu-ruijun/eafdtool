#!/usr/bin/python3
##################################################
'''总测试'''
# Copyright (c) 2022 Xu Ruijun | 1687701765@qq.com
# the file is MIT License,
# will change to GPLv3 in future.
##################################################
import unittest
from test.p_str import *

__author__ = "Xu Ruijun"
__copyright__ = "Copyright (c) 2022 Xu Ruijun"
__license__ = "MIT"


if __name__ == '__main__':
    unittest.main(verbosity=2)
